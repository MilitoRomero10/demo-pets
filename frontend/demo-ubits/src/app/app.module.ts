import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MiddlePanelComponent } from './components/middle-panel/middle-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    MiddlePanelComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
