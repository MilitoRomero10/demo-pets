insert into pet_type(description) values('Cat');
insert into pet_type(description) values('Dog');
insert into pet_type(description) values('Fish');
insert into pet_type(description) values('Other');

insert into size_pet(description) values('Small');
insert into size_pet(description) values('Medium');
insert into size_pet(description) values('Big');

insert into service_type(description) values('Medical');
insert into service_type(description) values('Vaccination');
insert into service_type(description) values('Deworming');
insert into service_type(description) values('Bath');
insert into service_type(description) values('Haircut');
insert into service_type(description) values('Toys');
insert into service_type(description) values('Clothes');

insert into vaccine_type(description) values('Type One');
insert into vaccine_type(description) values('Type Two');
insert into vaccine_type(description) values('Type Three');
