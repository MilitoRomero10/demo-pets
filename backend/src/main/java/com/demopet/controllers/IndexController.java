package com.demopet.controllers;

import com.demopet.dtos.MedicalRecordDTO;
import com.demopet.model.MedicalRecord;
import com.demopet.services.MedicalRecordService;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/medicalRecords/")
public class IndexController {

    private final MedicalRecordService medicalRecordService;

    public IndexController(MedicalRecordService medicalRecordService) {
        this.medicalRecordService = medicalRecordService;
    }

    @RequestMapping({"/index"})
    public Set<MedicalRecord> getIndexPage(){
        Set<MedicalRecord> medicalRecords = new HashSet<>();
        try {

            return medicalRecordService.getMedicalReports();
//            model.addAttribute("medicalRecords", medicalRecords);
        }catch (Exception e){
            e.printStackTrace();
        }
        return medicalRecords;
//        return "index.xhtml";
    }

    @PostMapping({"create","/create"})
    public String createMedicalRecord(@RequestBody MedicalRecordDTO medicalRecordDTO){
        try {
            // template
            medicalRecordService.save(parse(medicalRecordDTO));
        }catch (Exception e){
            e.printStackTrace();
        }
        return "index.xhtml";
    }

    @PutMapping("/{id}")
    public String updateMedicalRecord(@PathVariable Long id, @RequestBody MedicalRecordDTO medicalRecordDTO){
        try {
            MedicalRecord medicalRecord = medicalRecordService.findById(id).get();
            medicalRecord.setDescription(medicalRecordDTO.getDescription());
            medicalRecord.setPet(medicalRecordDTO.getPet());
            medicalRecord.setVaccines(medicalRecordDTO.getVaccines());
            medicalRecordService.save(medicalRecord);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    @DeleteMapping("/{id}")
    public String deleteMedicalRecord(@PathVariable Long id){
        try {
            medicalRecordService.deleteById(id);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public MedicalRecord parse(MedicalRecordDTO medicalRecordDTO){
        MedicalRecord medicalRecord = new MedicalRecord();
        medicalRecord.setDescription(medicalRecordDTO.getDescription());
        medicalRecord.setPet(medicalRecordDTO.getPet());
        medicalRecord.setVaccines(medicalRecordDTO.getVaccines());
        return medicalRecord;
    }


}
