package com.demopet.dtos;

import com.demopet.model.Pet;
import com.demopet.model.Vaccine;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class MedicalRecordDTO {
        private String description;
        private Pet pet;
        private Set<Vaccine> vaccines = new HashSet<>();

        public MedicalRecordDTO() {
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Pet getPet() {
            return pet;
        }

        public void setPet(Pet pet) {
            this.pet = pet;
        }

        public Set<Vaccine> getVaccines() {
            return vaccines;
        }

        public void setVaccines(Set<Vaccine> vaccines) {
            this.vaccines = vaccines;
        }
    }

