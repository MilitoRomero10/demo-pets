package com.demopet.services;

import com.demopet.exceptions.AppException;
import com.demopet.model.PetType;

import java.util.Optional;
import java.util.Set;

public interface PetTypeService {

    Set<PetType> getPetTypes() throws AppException;
    PetType save(PetType petType) throws AppException;
    Optional<PetType> findById(Long id) throws AppException;
    void deleteById(Long id) throws AppException;
    void delete(PetType petType);

}
