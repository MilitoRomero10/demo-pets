package com.demopet.services;

import com.demopet.exceptions.AppException;
import com.demopet.model.Pet;

import java.util.Optional;
import java.util.Set;

public interface PetService {
    Set<Pet> getPets() throws AppException;
    Pet save(Pet pet) throws AppException;
    Optional<Pet> findById(Long id) throws AppException;
    void deleteById(Long id) throws AppException;
    void delete(Pet pet);
}
