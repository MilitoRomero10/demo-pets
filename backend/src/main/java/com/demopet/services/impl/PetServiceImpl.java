package com.demopet.services.impl;

import com.demopet.exceptions.AppException;
import com.demopet.model.Pet;
import com.demopet.repositories.PetRepository;
import com.demopet.services.PetService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class PetServiceImpl implements PetService {

    private final PetRepository petRepository;

    public PetServiceImpl(PetRepository petRepository) {
        this.petRepository = petRepository;
    }

    @Override
    public Set<Pet> getPets() throws AppException {
        Set<Pet> pets = new HashSet<>();
        petRepository.findAll().iterator().forEachRemaining(pets::add);
        return pets;
    }

    @Override
    public Pet save(Pet pet) throws AppException {
        return petRepository.save(pet);
    }

    @Override
    public Optional<Pet> findById(Long id) throws AppException {
        return petRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) throws AppException {
        petRepository.deleteById(id);
    }

    @Override
    public void delete(Pet pet) {
        petRepository.delete(pet);
    }
}
