package com.demopet.services.impl;

import com.demopet.exceptions.AppException;
import com.demopet.model.MedicalRecord;
import com.demopet.repositories.MedicalRecordRepository;
import com.demopet.services.MedicalRecordService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@Service
public class MedicalRecordServiceImpl implements MedicalRecordService {

    private final MedicalRecordRepository medicalRecordRepository;

    public MedicalRecordServiceImpl(MedicalRecordRepository medicalRecordRepository) {
        this.medicalRecordRepository = medicalRecordRepository;
    }

    public Optional<MedicalRecord> findById(Long id) throws AppException {
        return medicalRecordRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) throws AppException {
        medicalRecordRepository.deleteById(id);
    }

    @Override
    public void delete(MedicalRecord medicalRecord) {
        medicalRecordRepository.delete(medicalRecord);
    }

    public Set<MedicalRecord> getMedicalReports() throws AppException {
        Set<MedicalRecord> medicalRecords = new HashSet<>();
        medicalRecordRepository.findAll().iterator().forEachRemaining(medicalRecords::add);
        return medicalRecords;
    }

    @Override
    public MedicalRecord save(MedicalRecord medicalRecord) throws AppException {
        return medicalRecordRepository.save(medicalRecord);
    }
}
