package com.demopet.services.impl;

import com.demopet.exceptions.AppException;
import com.demopet.model.Owner;
import com.demopet.repositories.OwnerRepository;
import com.demopet.services.OwnerService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class OwnerServiceImpl implements OwnerService {

    private final OwnerRepository ownerRepository;

    public OwnerServiceImpl(OwnerRepository ownerRepository) {
        this.ownerRepository = ownerRepository;
    }

    @Override
    public Set<Owner> getOwners() throws AppException {
        Set<Owner> owners = new HashSet<>();
        ownerRepository.findAll().iterator().forEachRemaining(owners::add);
        return owners;
    }

    @Override
    public Owner save(Owner owner) throws AppException {
        return ownerRepository.save(owner);
    }

    @Override
    public Optional<Owner> findById(Long id) throws AppException {
        return ownerRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) throws AppException {
        ownerRepository.deleteById(id);
    }

    @Override
    public void delete(Owner owner) {
        ownerRepository.delete(owner);
    }
}
