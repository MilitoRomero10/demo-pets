package com.demopet.services.impl;

import com.demopet.exceptions.AppException;
import com.demopet.model.PetType;
import com.demopet.repositories.PetTypeRepository;
import com.demopet.services.PetTypeService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class PetTypeServiceImpl implements PetTypeService {

    private final PetTypeRepository petTypeRepository;

    public PetTypeServiceImpl(PetTypeRepository petTypeRepository) {
        this.petTypeRepository = petTypeRepository;
    }

    @Override
    public Set<PetType> getPetTypes() throws AppException {
        Set<PetType> petTypeSet = new HashSet<>();
        petTypeRepository.findAll().iterator().forEachRemaining(petTypeSet::add);
        return petTypeSet;
    }

    @Override
    public PetType save(PetType petType) throws AppException {
        return petTypeRepository.save(petType);
    }

    @Override
    public Optional<PetType> findById(Long id) throws AppException {
        return petTypeRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) throws AppException {
        petTypeRepository.deleteById(id);
    }

    @Override
    public void delete(PetType petType) {
        petTypeRepository.delete(petType);
    }
}
