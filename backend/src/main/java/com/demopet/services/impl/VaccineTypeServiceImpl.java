package com.demopet.services.impl;

import com.demopet.exceptions.AppException;
import com.demopet.model.VaccineType;
import com.demopet.repositories.VaccineTypeRepository;
import com.demopet.services.VaccineTypeService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class VaccineTypeServiceImpl implements VaccineTypeService {

    private final VaccineTypeRepository vaccineTypeRepository;

    public VaccineTypeServiceImpl(VaccineTypeRepository vaccineTypeRepository) {
        this.vaccineTypeRepository = vaccineTypeRepository;
    }

    @Override
    public Set<VaccineType> getVaccinesType() throws AppException {
        Set<VaccineType> vaccineTypes = new HashSet<>();
        vaccineTypeRepository.findAll().iterator().forEachRemaining(vaccineTypes :: add);
        return vaccineTypes;
    }

    @Override
    public VaccineType save(VaccineType vaccineType) throws AppException {
        return vaccineTypeRepository.save(vaccineType);
    }

    @Override
    public Optional<VaccineType> findById(Long id) throws AppException {
        return vaccineTypeRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) throws AppException {
        vaccineTypeRepository.deleteById(id);
    }

    @Override
    public void delete(VaccineType vaccineType) {
        vaccineTypeRepository.delete(vaccineType);
    }
}
