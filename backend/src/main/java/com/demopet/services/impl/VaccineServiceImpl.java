package com.demopet.services.impl;

import com.demopet.exceptions.AppException;
import com.demopet.model.Vaccine;
import com.demopet.repositories.VaccineRepository;
import com.demopet.services.VaccineService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class VaccineServiceImpl implements VaccineService {

    private final VaccineRepository vaccineRepository;

    public VaccineServiceImpl(VaccineRepository vaccineRepository) {
        this.vaccineRepository = vaccineRepository;
    }

    @Override
    public Set<Vaccine> getVaccines() throws AppException {
        Set<Vaccine> vaccines = new HashSet<>();
        vaccineRepository.findAll().iterator().forEachRemaining(vaccines::add);
        return vaccines;
    }

    @Override
    public Vaccine save(Vaccine vaccine) throws AppException {
        return vaccineRepository.save(vaccine);
    }

    @Override
    public Optional<Vaccine> findById(Long id) throws AppException {
        return vaccineRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) throws AppException {
        vaccineRepository.deleteById(id);
    }

    @Override
    public void delete(Vaccine vaccine) {
        vaccineRepository.delete(vaccine);
    }
}
