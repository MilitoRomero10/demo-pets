package com.demopet.services.impl;

import com.demopet.exceptions.AppException;
import com.demopet.model.SizePet;
import com.demopet.repositories.SizePetRepository;
import com.demopet.services.SizePetService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class SizePetServiceImpl implements SizePetService {

    private final SizePetRepository sizePetRepository;

    public SizePetServiceImpl(SizePetRepository sizePetRepository) {
        this.sizePetRepository = sizePetRepository;
    }

    @Override
    public Set<SizePet> getSizesPet() throws AppException {
        Set<SizePet> sizePets =  new HashSet<>();
        sizePetRepository.findAll().iterator().forEachRemaining(sizePets::add);
        return sizePets;
    }

    @Override
    public SizePet save(SizePet sizePet) throws AppException {
        return sizePetRepository.save(sizePet);
    }

    @Override
    public Optional<SizePet> findById(Long id) throws AppException {
        return sizePetRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) throws AppException {
        sizePetRepository.deleteById(id);
    }

    @Override
    public void delete(SizePet sizePet) {
        sizePetRepository.delete(sizePet);
    }
}
