package com.demopet.services.impl;

import com.demopet.exceptions.AppException;
import com.demopet.model.User;
import com.demopet.repositories.UserRepository;
import com.demopet.services.UserService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Set<User> getUsers() throws AppException {
        Set<User> users = new HashSet<>();
        userRepository.findAll().iterator().forEachRemaining(users::add);
        return users;
    }

    @Override
    public User save(User user) throws AppException {
        return userRepository.save(user);
    }

    @Override
    public Optional<User> findById(Long id) throws AppException {
        return userRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) throws AppException {
        userRepository.deleteById(id);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }
}
