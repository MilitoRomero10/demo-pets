package com.demopet.services;

import com.demopet.exceptions.AppException;
import com.demopet.model.MedicalRecord;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface MedicalRecordService {

    Set<MedicalRecord> getMedicalReports() throws AppException;

    MedicalRecord save(MedicalRecord medicalRecord) throws AppException;

    Optional<MedicalRecord> findById(Long id) throws AppException;

    void deleteById(Long id) throws AppException;

    void delete(MedicalRecord medicalRecord);
}
