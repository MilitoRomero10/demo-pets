package com.demopet.services;

import com.demopet.exceptions.AppException;
import com.demopet.model.Vaccine;

import java.util.Optional;
import java.util.Set;

public interface VaccineService {

    Set<Vaccine> getVaccines() throws AppException;
    Vaccine save(Vaccine vaccine) throws AppException;
    Optional<Vaccine> findById(Long id) throws AppException;
    void deleteById(Long id) throws AppException;
    void delete(Vaccine vaccine);

}
