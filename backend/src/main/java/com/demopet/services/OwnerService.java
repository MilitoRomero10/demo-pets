package com.demopet.services;

import com.demopet.exceptions.AppException;
import com.demopet.model.Owner;

import java.util.Optional;
import java.util.Set;

public interface OwnerService {
    Set<Owner> getOwners() throws AppException;
    Owner save(Owner owner) throws AppException;
    Optional<Owner> findById(Long id) throws AppException;
    void deleteById(Long id) throws AppException;
    void delete(Owner owner);
}
