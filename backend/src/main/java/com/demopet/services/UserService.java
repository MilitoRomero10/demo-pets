package com.demopet.services;

import com.demopet.exceptions.AppException;
import com.demopet.model.User;

import java.util.Optional;
import java.util.Set;

public interface UserService {
    Set<User> getUsers() throws AppException;
    User save(User user) throws AppException;
    Optional<User> findById(Long id) throws AppException;
    void deleteById(Long id) throws AppException;
    void delete(User user);
}
