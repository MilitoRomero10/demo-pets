package com.demopet.services;

import com.demopet.exceptions.AppException;
import com.demopet.model.SizePet;

import java.util.Optional;
import java.util.Set;

public interface SizePetService {

    Set<SizePet> getSizesPet() throws AppException;
    SizePet save(SizePet sizePet) throws AppException;
    Optional<SizePet> findById(Long id) throws AppException;
    void deleteById(Long id) throws AppException;
    void delete(SizePet sizePet);

}
