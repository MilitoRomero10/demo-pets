package com.demopet.services;

import com.demopet.exceptions.AppException;
import com.demopet.model.VaccineType;

import java.util.Optional;
import java.util.Set;

public interface VaccineTypeService {

    Set<VaccineType> getVaccinesType() throws AppException;
    VaccineType save(VaccineType vaccineType) throws AppException;
    Optional<VaccineType> findById(Long id) throws AppException;
    void deleteById(Long id) throws AppException;
    void delete(VaccineType vaccineType);

}
