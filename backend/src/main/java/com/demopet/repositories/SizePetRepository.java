package com.demopet.repositories;

import com.demopet.model.SizePet;
import org.springframework.data.repository.CrudRepository;

public interface SizePetRepository extends CrudRepository<SizePet, Long> {
}
