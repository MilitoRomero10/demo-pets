package com.demopet.repositories;

import com.demopet.model.VaccineType;
import org.springframework.data.repository.CrudRepository;

public interface VaccineTypeRepository extends CrudRepository<VaccineType, Long> {
}
