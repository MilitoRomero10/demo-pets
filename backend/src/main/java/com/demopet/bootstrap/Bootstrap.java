package com.demopet.bootstrap;

import com.demopet.model.*;
import com.demopet.services.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Component
public class Bootstrap implements CommandLineRunner {

    private final MedicalRecordService medicalRecordService;
    private final UserService userService;
    private final OwnerService ownerService;
    private final PetService petService;
    private final PetTypeService petTypeService;
    private final VaccineTypeService vaccineTypeService;
    private final VaccineService vaccineService;
    private final SizePetService sizePetService;

    public Bootstrap(MedicalRecordService medicalRecordService, UserService userService, OwnerService ownerService, PetService petService, PetTypeService petTypeService, VaccineTypeService vaccineTypeService, VaccineService vaccineService, SizePetService sizePetService) {
        this.medicalRecordService = medicalRecordService;
        this.userService = userService;
        this.ownerService = ownerService;
        this.petService = petService;
        this.petTypeService = petTypeService;
        this.vaccineTypeService = vaccineTypeService;
        this.vaccineService = vaccineService;
        this.sizePetService = sizePetService;
    }

    @Override
    public void run(String... args) throws Exception {

        Set<VaccineType> vaccineTypes = vaccineTypeService.getVaccinesType();
        Set<SizePet> sizePets = sizePetService.getSizesPet();

        Vaccine vaccine = new Vaccine();
        vaccine.setDate(new Date(System.currentTimeMillis()));
        vaccine.setNumber(1234453l);
        vaccine.setVaccineType(vaccineTypes.iterator().next());
        vaccine = vaccineService.save(vaccine);

        Set<PetType> petTypes = petTypeService.getPetTypes();

        User user = new User();
        user.setUsername("Abcd-123");
        user.setPassword("prueba123");
        user = userService.save(user);

        Owner owner = new Owner();
        owner.setUser(user);
        owner.setName("Propietario 1");
        owner = ownerService.save(owner);

        Pet pet = new Pet();
        pet.setOwner(owner);
        pet.setDescription("Pet description");
        pet.setPetType(petTypes.iterator().next());
        pet.setSize(sizePets.iterator().next());
        pet = petService.save(pet);


        MedicalRecord medicalRecord = new MedicalRecord();
        medicalRecord.setPet(pet);
        medicalRecord.setDescription("Medical Record");

        medicalRecord = medicalRecordService.save(medicalRecord);


    }
}
