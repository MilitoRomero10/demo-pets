package com.demopet.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "service")
public class Service extends BaseEntity {

    @Column(name = "description")
    private String description;

    @OneToOne
    private ServiceType serviceType;

    public Service() {
    }

    public Service(String description, ServiceType serviceType) {
        this.description = description;
        this.serviceType = serviceType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }
}
