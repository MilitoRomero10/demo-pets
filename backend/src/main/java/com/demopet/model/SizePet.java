package com.demopet.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@Entity(name = "size_pet")
public class SizePet extends BaseEntity{

    @Column(name = "description")
    private String description;

    public SizePet() {
    }

    public SizePet(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
