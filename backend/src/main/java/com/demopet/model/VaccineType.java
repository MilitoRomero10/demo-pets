package com.demopet.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@Entity(name = "vaccine_type")
public class VaccineType extends BaseEntity{

    @Column(name = "description")
    private String description;

    public VaccineType() {
    }

    public VaccineType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
