package com.demopet.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@Entity(name = "service_type")
public class ServiceType extends BaseEntity{

    @Column(name = "description")
    private String description;

    public ServiceType() {
    }

    public ServiceType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
