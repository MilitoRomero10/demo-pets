package com.demopet.model;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity(name = "medical_record")
public class MedicalRecord extends BaseEntity{

    @Column(name = "description")
    private String description;

    @ManyToOne
    private Pet pet;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medicalRecord")
    private Set<Vaccine> vaccines = new HashSet<>();

    public MedicalRecord() {
    }

    public MedicalRecord(String description, Pet pet, Set<Vaccine> vaccines) {
        this.description = description;
        this.pet = pet;
        this.vaccines = vaccines;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Set<Vaccine> getVaccines() {
        return vaccines;
    }

    public void setVaccines(Set<Vaccine> vaccines) {
        this.vaccines = vaccines;
    }
}
