package com.demopet.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity(name = "vaccine")
public class Vaccine extends BaseEntity {

    @Column(name = "number")
    private Long number;

    @OneToOne
    private VaccineType vaccineType;

    @CreationTimestamp
    private Date date;

    @ManyToOne
    private MedicalRecord medicalRecord;

    public Vaccine() {
    }

    public Vaccine(Long number, VaccineType vaccineType, Date date, MedicalRecord medicalRecord) {
        this.number = number;
        this.vaccineType = vaccineType;
        this.date = date;
        this.medicalRecord = medicalRecord;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public VaccineType getVaccineType() {
        return vaccineType;
    }

    public void setVaccineType(VaccineType vaccineType) {
        this.vaccineType = vaccineType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public MedicalRecord getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(MedicalRecord medicalRecord) {
        this.medicalRecord = medicalRecord;
    }
}
