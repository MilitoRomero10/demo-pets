package com.demopet.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@Entity(name = "pet_type")
public class PetType extends  BaseEntity{

    @Column(name = "description")
    private String description;

    public PetType() {
    }

    public PetType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
