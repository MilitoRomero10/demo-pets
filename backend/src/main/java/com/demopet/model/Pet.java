package com.demopet.model;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity(name = "pet")
public class Pet extends BaseEntity{


    @Column(name = "description")
    private String description;

    @OneToOne
    private PetType petType;

    @OneToOne
    private SizePet size;

    @ManyToOne
    private Owner owner;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pet")
    private Set<MedicalRecord> medicalRecord = new HashSet<>();

    public Pet() {}

    public Pet(String description, PetType petType, SizePet size, Owner owner, Set<MedicalRecord> medicalRecord) {
        this.description = description;
        this.petType = petType;
        this.size = size;
        this.owner = owner;
        this.medicalRecord = medicalRecord;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PetType getPetType() {
        return petType;
    }

    public void setPetType(PetType petType) {
        this.petType = petType;
    }

    public SizePet getSize() {
        return size;
    }

    public void setSize(SizePet size) {
        this.size = size;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Set<MedicalRecord> getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(Set<MedicalRecord> medicalRecord) {
        this.medicalRecord = medicalRecord;
    }
}
